package com.test.securepim.screens.list.loader;

import android.content.Context;
import android.database.Cursor;

import com.test.securepim.db.ThumbnailsContract;
import com.test.securepim.loaders.AsyncLoader;
import com.test.securepim.models.Thumbnail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andrii on 27/03/2017.
 */

public class ThumbnailsLoader extends AsyncLoader<List<Thumbnail>> {
    /**
     * Construct loader
     *
     * @param context of application
     */
    public ThumbnailsLoader(final Context context) {
        super(context);
    }

    @Override
    public List<Thumbnail> loadInBackground() {
        final Cursor query = getContext().getContentResolver().query(ThumbnailsContract.THUMBNAILS_URI, null, null, null, null);
        List<Thumbnail> result = null;
        if (query != null) {
            if (query.moveToFirst()) {
                result = new ArrayList<>(query.getCount());
                do {
                    result.add(Thumbnail.create(query));
                } while (query.moveToNext());
            }
            query.close();
        }
        return result;
    }
}
