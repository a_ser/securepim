package com.test.securepim.screens.list;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.Loader;

import com.test.securepim.base.presenter.BasePresenter;
import com.test.securepim.base.resources.ResourcesManagerFactory;
import com.test.securepim.models.Thumbnail;
import com.test.securepim.utils.Constants;
import com.test.securepim.utils.Func1;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import timber.log.Timber;

/**
 * Created by andrii on 27/03/2017.
 */

class ListPresenter extends BasePresenter<ListFragment> {
    static final String SEARCH_KEY = "search";
    static String DEFAULT_SEARCH_KEY = "fruit";
    private final ListInteractor mInteractor;
    @NonNull
    private String mSearchWord;

    ListPresenter(@NonNull final ListFragment view, @NonNull final ResourcesManagerFactory resourcesManagerFactory, @NonNull final String searchWord, final Func1<Loader<List<Thumbnail>>> thumbnailAsyncLoaderFunc) {
        super(view);
        mSearchWord = searchWord;
        mInteractor = new ListInteractor(resourcesManagerFactory, thumbnailAsyncLoaderFunc);
    }

    /**
     * Load data. Set {@param isForcedReload} as true to force internet request
     *
     * @param isForcedReload true if should request internet. Otherwise, will trigger cache
     */
    void loadData(final boolean isForcedReload) {
        search(mSearchWord, isForcedReload);
    }

    private void search(@NonNull final String query, final boolean isForcedReload) {
        mSearchWord = query;
        try {
            final String encodedQuery = URLEncoder.encode(query, Constants.UTF_8);
            mInteractor.loadList(thumbnails -> {
                if (mView != null) {
                    if (thumbnails != null && !thumbnails.isEmpty()) {
                        mView.updateThumbnail(thumbnails);
                    } else {
                        mView.showNoDataAvailable();
                    }
                }
            }, this::managetLoadingVisibility, encodedQuery, isForcedReload);
        } catch (UnsupportedEncodingException e) {
            Timber.e(e);
            //TODO notify user
        }

    }

    void search(@NonNull final String query) {
        search(query, true);
    }

    void registerContentResolver() {
        mInteractor.registerContentResolver();
    }

    void unregisterContentResolver() {
        mInteractor.unregisterContentResolver();
    }

    void unregisterCallbacks() {
        mInteractor.unregisterCallback();
    }

    void saveInstance(@Nullable final Bundle outState) {
        if (outState != null) {
            outState.putString(SEARCH_KEY, mSearchWord);
        }
    }
}
