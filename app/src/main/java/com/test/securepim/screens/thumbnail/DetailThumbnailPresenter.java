package com.test.securepim.screens.thumbnail;

import android.support.annotation.NonNull;
import android.support.v4.content.Loader;

import com.test.securepim.base.presenter.BasePresenter;
import com.test.securepim.base.resources.ResourcesManagerFactory;
import com.test.securepim.models.Thumbnail;
import com.test.securepim.utils.Constants;
import com.test.securepim.utils.Func2;

/**
 * Created by andrii on 28/03/2017.
 */

class DetailThumbnailPresenter extends BasePresenter<DetailThumbnailFragment> {

    DetailThumbnailPresenter(@NonNull final DetailThumbnailFragment view, @NonNull final ResourcesManagerFactory resourcesManagerFactory, @NonNull final Func2<Integer, Loader<Thumbnail>> thumbnailAsyncLoaderFunc, final int thumbnailId) {
        super(view);
        final DetailThumbnailInteractor interactor = new DetailThumbnailInteractor(resourcesManagerFactory, thumbnailAsyncLoaderFunc);
        if (thumbnailId == Constants.NO_VALUE_RECEIVED) {
            view.showNoData();
        } else {
            interactor.loadThumbnail(thumbnailId, thumbnail -> {
                if (mView != null) {
                    if (thumbnail != null) {
                        mView.setThumbnail(thumbnail);
                    } else {
                        mView.showNoData();
                    }
                }
            });
        }
    }
}
