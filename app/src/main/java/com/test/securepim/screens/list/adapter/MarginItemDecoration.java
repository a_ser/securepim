package com.test.securepim.screens.list.adapter;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by andrii on 27/03/2017.
 */

public class MarginItemDecoration extends RecyclerView.ItemDecoration {
    @Override
    public void getItemOffsets(final Rect outRect, final View view, final RecyclerView parent, final RecyclerView.State state) {
        final int space = 20;
        outRect.left = space;
        outRect.right = space;
        outRect.bottom = 2 * space;
    }
}
