package com.test.securepim.screens.list;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;

import com.test.securepim.R;
import com.test.securepim.models.Thumbnail;
import com.test.securepim.utils.Constants;

/**
 * Created by andrii on 28/03/2017.
 */

public class ThumbnailConfirmationDialog extends DialogFragment {
    public static final String THUMBNAIL_CLICK_ACTION = "THUMBNAIL_CLICK_ACTION";
    public static final String TAG = "ThumbnailConfirmationDialog";
    public static final IntentFilter INTENT_FILTER = new IntentFilter(THUMBNAIL_CLICK_ACTION);
    private static final String THUMBNAIL_ID_KEY = "THUMBNAIL_ID";

    public static ThumbnailConfirmationDialog create(@NonNull final Thumbnail thumbnail) {
        final ThumbnailConfirmationDialog dialog = new ThumbnailConfirmationDialog();
        final Bundle bundle = new Bundle(1);
        bundle.putInt(THUMBNAIL_ID_KEY, thumbnail.id);
        dialog.setArguments(bundle);
        return dialog;
    }

    public static int getIdFromIntent(@NonNull final Intent intent) {
        return intent.getIntExtra(THUMBNAIL_ID_KEY, Constants.NO_VALUE_RECEIVED);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        final Bundle arguments = getArguments();
        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.thumbnail_confirmation_dialog_title)
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> dismiss())
                .setPositiveButton(R.string.thumbnail_confirmation_dialog_open_button,
                        (dialog, whichButton) -> {
                            Intent intent = new Intent(THUMBNAIL_CLICK_ACTION);
                            intent.putExtras(arguments);
                            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
                        }
                )
                .setCancelable(true)
                .create();
    }
}
