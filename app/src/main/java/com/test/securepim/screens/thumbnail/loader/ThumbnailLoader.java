package com.test.securepim.screens.thumbnail.loader;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.test.securepim.db.ThumbnailsContract;
import com.test.securepim.loaders.AsyncLoader;
import com.test.securepim.models.Thumbnail;

/**
 * Created by andrii on 27/03/2017.
 */

public class ThumbnailLoader extends AsyncLoader<Thumbnail> {
    private final int mThumbId;

    /**
     * Construct loader
     *
     * @param context of application
     */
    public ThumbnailLoader(@NonNull final Context context, final int thumbId) {
        super(context);
        mThumbId = thumbId;
    }

    @Override
    public Thumbnail loadInBackground() {
        final String selection = ThumbnailsContract.ThumbnailsEntry.COLUMN_ID + " = ?";
        final Cursor query = getContext().getContentResolver().query(ThumbnailsContract.THUMBNAILS_URI_ITEM, null, selection, new String[]{String.valueOf(mThumbId)}, null);
        Thumbnail result = null;
        if (query != null) {
            if (query.moveToFirst()) {
                result = Thumbnail.create(query);
            }
            query.close();
        }
        return result;
    }
}
