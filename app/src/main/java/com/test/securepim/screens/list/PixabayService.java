package com.test.securepim.screens.list;

import android.support.annotation.NonNull;

import com.test.securepim.models.PixabayResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by andrii on 27/03/2017.
 */

interface PixabayService {
    @GET("api/")
    Call<PixabayResponse> getThumnails(@QueryMap @NonNull Map<String, String> option);
}
