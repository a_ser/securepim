package com.test.securepim.screens.list;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.test.securepim.base.interactor.BaseInteractor;
import com.test.securepim.base.resources.ResourcesManagerFactory;
import com.test.securepim.db.ThumbnailsContract;
import com.test.securepim.models.PixabayResponse;
import com.test.securepim.models.Thumbnail;
import com.test.securepim.net.InternetManager;
import com.test.securepim.net.RequestData;
import com.test.securepim.utils.Action1;
import com.test.securepim.utils.Constants;
import com.test.securepim.utils.Func1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by andrii on 27/03/2017.
 */

class ListInteractor extends BaseInteractor implements LoaderManager.LoaderCallbacks<List<Thumbnail>> {
    private static final String KEY_PARAM = "key";
    private static final String QUERY_PARAM = "q";
    private final ThumbnailsObserver mThumbnailsObserver;
    @NonNull
    private final Func1<Loader<List<Thumbnail>>> mThumbnailAsyncLoaderFunc;
    @NonNull
    private Map<RequestData, Callback<PixabayResponse>> mCallbacks = new HashMap<>();
    private Action1<List<Thumbnail>> mListDownloadedAction;

    ListInteractor(@NonNull final ResourcesManagerFactory resourcesManagerFactory, @NonNull final Func1<Loader<List<Thumbnail>>> thumbnailsAsyncLoaderFunc) {
        super(resourcesManagerFactory);
        mThumbnailAsyncLoaderFunc = thumbnailsAsyncLoaderFunc;
        mThumbnailsObserver = new ThumbnailsObserver(this);
    }

    void loadList(@NonNull final Action1<List<Thumbnail>> listDownloadedAction, @NonNull final Action1<Boolean> loadingVisibilityAction, @NonNull final String searchStringEncoded, final boolean isForcedReload) {
        mListDownloadedAction = listDownloadedAction;
        final InternetManager internetManager = getResourcesManagerFactory().getInternetManager();
        final Map<String, String> params = new HashMap<>();
        params.put(KEY_PARAM, Constants.PIXABAY_API_KEY);
        params.put(QUERY_PARAM, searchStringEncoded);
        final RequestData requestData = RequestData.create(Constants.PIXABAY_BASE_URL, params);
        final Callback<PixabayResponse> callback = new Callback<PixabayResponse>() {
            @Override
            public void onResponse(final Call<PixabayResponse> call, final Response<PixabayResponse> response) {
                final PixabayResponse pixabayResponse = response.body();
                if (pixabayResponse != null) {
                    getResourcesManagerFactory().getContentResolver().clearAndInsert(ThumbnailsContract.THUMBNAILS_URI, pixabayResponse.getValues());
                    loadingVisibilityAction.call(false);
                }
                mCallbacks.values().remove(this);
            }

            @Override
            public void onFailure(final Call<PixabayResponse> call, final Throwable t) {
                Timber.w(t);
                loadingVisibilityAction.call(false);
                mCallbacks.values().remove(this);
                //todo show error to user
            }
        };

        // Load data from internet for the same url only once
        if (internetManager.isRequesting(requestData)) {
            mCallbacks.put(requestData, callback);
            internetManager.register(requestData, callback);
            loadingVisibilityAction.call(true);
        } else if (isForcedReload) {
            mCallbacks.put(requestData, callback);
            internetManager.makeRequest(requestData, PixabayService.class, pixabayService -> pixabayService.getThumnails(requestData.getParams()), mCallbacks.get(requestData));
            loadingVisibilityAction.call(true);
        } else {
            loadingVisibilityAction.call(false);
        }
        getResourcesManagerFactory().getLoaderManager().initLoader(1, null, this);
    }

    void registerContentResolver() {
        getResourcesManagerFactory().getContentResolver().registerContentObserver(ThumbnailsContract.THUMBNAILS_URI, false, mThumbnailsObserver);
    }

    void unregisterContentResolver() {
        getResourcesManagerFactory().getContentResolver().unregisterContentObserver(mThumbnailsObserver);
    }

    void unregisterCallback() {
        getResourcesManagerFactory().getInternetManager().unregister(mCallbacks);
    }

    @Override
    public Loader<List<Thumbnail>> onCreateLoader(final int id, final Bundle args) {
        return mThumbnailAsyncLoaderFunc.call();
    }

    @Override
    public void onLoadFinished(final Loader<List<Thumbnail>> loader, final List<Thumbnail> data) {
        mListDownloadedAction.call(data);
    }

    @Override
    public void onLoaderReset(final Loader<List<Thumbnail>> loader) {
        mListDownloadedAction.call(null);
    }

    private class ThumbnailsObserver extends ContentObserver {
        private final LoaderManager.LoaderCallbacks<List<Thumbnail>> mCallbacks;

        ThumbnailsObserver(LoaderManager.LoaderCallbacks<List<Thumbnail>> callbacks) {
            this(new Handler(), callbacks);
        }

        ThumbnailsObserver(Handler handler, LoaderManager.LoaderCallbacks<List<Thumbnail>> callbacks) {
            super(handler);
            mCallbacks = callbacks;
        }

        @Override
        public void onChange(boolean selfChange) {
            this.onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            getResourcesManagerFactory().getLoaderManager().restartLoader(1, null, mCallbacks);
        }
    }
}
