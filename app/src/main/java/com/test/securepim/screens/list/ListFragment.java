package com.test.securepim.screens.list;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.securepim.R;
import com.test.securepim.base.view.BaseFragment;
import com.test.securepim.models.Thumbnail;
import com.test.securepim.screens.list.adapter.ListAdapter;
import com.test.securepim.screens.list.adapter.MarginItemDecoration;
import com.test.securepim.screens.list.loader.ThumbnailsLoader;
import com.test.securepim.screens.thumbnail.DetailThumbnailFragment;

import java.util.List;

/**
 * Created by andrii on 27/03/2017.
 */

public class ListFragment extends BaseFragment<ListPresenter, ListFragment> {
    private final BroadcastReceiver mThumbnailConfirmationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            navigateToFragment(DetailThumbnailFragment.create(ThumbnailConfirmationDialog.getIdFromIntent(intent)));
        }
    };
    @Nullable
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ListAdapter mListAdapter;
    private TextView mSupportTextView;

    public static Fragment create() {
        return new ListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_list, container, false);
        initViews(view);
        return view;
    }

    private void initViews(@NonNull final View view) {
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.fragment_list_swipe_to_refresh_layout);
        mSupportTextView = (TextView) view.findViewById(R.id.fragment_list_support_view);
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setOnRefreshListener(() -> {
                ListPresenter presenter = getPresenter();
                if (presenter != null) {
                    presenter.loadData(true);
                }
            });
        }
        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.fragment_list_recycler_view);
        mListAdapter = new ListAdapter(this::showOpenThumbnailDialog);
        // TODO change for tablet
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(view.getContext(), getResources().getInteger(R.integer.grid_image_count));
        recyclerView.addItemDecoration(new MarginItemDecoration());
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(mListAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        final ListPresenter presenter = getPresenter();
        if (presenter != null) {
            presenter.registerContentResolver();
        }
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mThumbnailConfirmationReceiver, ThumbnailConfirmationDialog.INTENT_FILTER);
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        final ListPresenter presenter = getPresenter();
        if (presenter != null) {
            presenter.saveInstance(outState);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        final ListPresenter presenter = getPresenter();
        if (presenter != null) {
            presenter.unregisterContentResolver();
            presenter.unregisterCallbacks();
        }
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mThumbnailConfirmationReceiver);
    }

    private void showOpenThumbnailDialog(final Thumbnail thumbnail) {
        ThumbnailConfirmationDialog thumbnailConfirmationDialog = (ThumbnailConfirmationDialog) getFragmentManager().findFragmentByTag(ThumbnailConfirmationDialog.TAG);
        if (thumbnailConfirmationDialog == null) {
            thumbnailConfirmationDialog = ThumbnailConfirmationDialog.create(thumbnail);
            thumbnailConfirmationDialog.show(getFragmentManager(), ThumbnailConfirmationDialog.TAG);
        }
    }

    @Override
    public void manageLoadingVisibility(final boolean isVisible) {

        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(isVisible);
        }
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final ListPresenter presenter = getPresenter();
        if (presenter != null) {
            presenter.loadData(savedInstanceState == null);
        }
    }

    @Override
    protected void initPresenter(@Nullable final Bundle savedInstanceState) {
        final String searchWord = savedInstanceState != null ? savedInstanceState.getString(ListPresenter.SEARCH_KEY, ListPresenter.DEFAULT_SEARCH_KEY) : ListPresenter.DEFAULT_SEARCH_KEY;
        setPresenter(new ListPresenter(this, getResourcesManagerFactory(), searchWord, () -> new ThumbnailsLoader(getContext())));
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_list_menu, menu);

        SearchView search = (SearchView) menu.findItem(R.id.list_menu_search).getActionView();

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {
                final ListPresenter presenter = getPresenter();
                if (presenter != null) {
                    presenter.search(query);
                    return true;
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                return false;
            }
        });
    }

    @Nullable
    @Override
    protected String getTitle() {
        return getString(R.string.fragment_list_title);
    }

    public void updateThumbnail(@NonNull final List<Thumbnail> thumbnails) {
        mListAdapter.setThumbnails(thumbnails);
        mListAdapter.notifyDataSetChanged();
        mSupportTextView.setVisibility(View.GONE);
    }

    public void showNoDataAvailable() {
        mListAdapter.clear();
        mSupportTextView.setText(R.string.no_data_available);
        mSupportTextView.setVisibility(View.VISIBLE);
    }
}
