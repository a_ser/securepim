package com.test.securepim.screens.thumbnail;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.test.securepim.R;
import com.test.securepim.base.view.BaseFragment;
import com.test.securepim.models.Thumbnail;
import com.test.securepim.screens.thumbnail.loader.ThumbnailLoader;
import com.test.securepim.utils.Constants;

/**
 * Created by andrii on 28/03/2017.
 */

public class DetailThumbnailFragment extends BaseFragment<DetailThumbnailPresenter, DetailThumbnailFragment> {
    private static final String THUMBNAIL_ID = "THUMBNAIL_ID";
    private ImageView mThumbnailView;
    private TextView mLikesView;
    private TextView mCommentsView;
    private TextView mFavoritesView;
    private TextView mUserNameView;
    private TextView mTagsView;
    private ImageView mUserImageView;
    private TextView mSupportView;

    public static DetailThumbnailFragment create(final int thumbnailId) {
        final DetailThumbnailFragment detailThumbnailFragment = new DetailThumbnailFragment();
        final Bundle bundle = new Bundle(1);
        bundle.putInt(THUMBNAIL_ID, thumbnailId);
        detailThumbnailFragment.setArguments(bundle);
        return detailThumbnailFragment;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_detail_thumbnail, container, false);
        initViews(view);
        return view;
    }

    private void initViews(@NonNull final View view) {
        mThumbnailView = (ImageView) view.findViewById(R.id.fragment_detail_thumbnail_image);
        mLikesView = (TextView) view.findViewById(R.id.fragment_detail_thumbnail_likes);
        mCommentsView = (TextView) view.findViewById(R.id.fragment_detail_thumbnail_comments);
        mFavoritesView = (TextView) view.findViewById(R.id.fragment_detail_thumbnail_favorites);
        mUserNameView = (TextView) view.findViewById(R.id.fragment_detail_thumbnail_user_name);
        mUserImageView = (ImageView) view.findViewById(R.id.fragment_detail_thumbnail_user_image);
        mTagsView = (TextView) view.findViewById(R.id.fragment_detail_thumbnail_tags);
        mSupportView = (TextView) view.findViewById(R.id.fragment_detail_thumbnail_support_view);
    }

    @Override
    protected void initPresenter(@Nullable final Bundle savedInstanceState) {
        final Bundle arguments = getArguments();
        final int thumbnailId = arguments != null ? arguments.getInt(THUMBNAIL_ID, Constants.NO_VALUE_RECEIVED) : Constants.NO_VALUE_RECEIVED;
        setPresenter(new DetailThumbnailPresenter(this, getResourcesManagerFactory(), thumbId -> new ThumbnailLoader(getContext(), thumbId), thumbnailId));
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Nullable
    @Override
    protected String getTitle() {
        return getContext().getString(R.string.detail_thumbnail_screen_title);
    }

    public void showNoData() {
        mSupportView.setVisibility(View.VISIBLE);
        mSupportView.setText(R.string.no_data_available);
    }

    public void setThumbnail(@NonNull final Thumbnail thumbnail) {
        mSupportView.setVisibility(View.GONE);
        Glide.with(getContext())
                .load(thumbnail.webformatURL)
                .centerCrop()
                .crossFade()
                .placeholder(R.drawable.placeholder)
                .into(mThumbnailView);
        Glide.with(getContext())
                .load(thumbnail.userImageURL)
                .asBitmap()
                .placeholder(R.drawable.placeholder)
                .centerCrop()
                .into(new BitmapImageViewTarget(mUserImageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        final Context context = getContext();
                        if (context != null) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            mUserImageView.setImageDrawable(circularBitmapDrawable);
                        }
                    }
                });
        mLikesView.setText(String.valueOf(thumbnail.likes));
        mCommentsView.setText(String.valueOf(thumbnail.comments));
        mFavoritesView.setText(String.valueOf(thumbnail.favorites));
        mUserNameView.setText(thumbnail.user);
        mTagsView.setText(thumbnail.getFormattedTags());
    }
}
