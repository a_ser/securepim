package com.test.securepim.screens.list.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.test.securepim.R;
import com.test.securepim.models.Thumbnail;
import com.test.securepim.utils.Action1;

import java.util.List;

/**
 * Created by andrii on 27/03/2017.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder> {
    private final Action1<Thumbnail> mThumbnailClickAction;
    private List<Thumbnail> mThumbnails;

    public ListAdapter(final Action1<Thumbnail> thumbnailClickAction) {
        mThumbnailClickAction = thumbnailClickAction;
    }

    public void setThumbnails(@NonNull final List<Thumbnail> thumbnails) {
        mThumbnails = thumbnails;
    }

    @Override
    public ListViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_list, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ListViewHolder holder, final int position) {
        final Thumbnail thumbnail = mThumbnails.get(position);
        final ImageView thumbnailImageView = holder.imageView;
        Glide.with(thumbnailImageView.getContext())
                .load(thumbnail.webformatURL)
                .centerCrop()
                .crossFade()
                .placeholder(R.drawable.placeholder)
                .into(thumbnailImageView);
        holder.userNameView.setText(thumbnail.user);
        holder.tagsView.setText(thumbnail.getFormattedTags());
        holder.itemView.setOnClickListener(view -> mThumbnailClickAction.call(thumbnail));
    }

    @Override
    public int getItemCount() {
        return mThumbnails == null ? 0 : mThumbnails.size();
    }

    public void clear() {
        if (mThumbnails != null) {
            mThumbnails.clear();
            notifyDataSetChanged();
        }
    }

    static class ListViewHolder extends RecyclerView.ViewHolder {
        final ImageView imageView;
        final TextView userNameView;
        final TextView tagsView;

        ListViewHolder(final View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.view_holder_image_view);
            userNameView = (TextView) itemView.findViewById(R.id.view_holder_user_name);
            tagsView = (TextView) itemView.findViewById(R.id.view_holder_tags);
        }
    }
}
