package com.test.securepim.screens.thumbnail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.test.securepim.base.interactor.BaseInteractor;
import com.test.securepim.base.resources.ResourcesManagerFactory;
import com.test.securepim.models.Thumbnail;
import com.test.securepim.utils.Action1;
import com.test.securepim.utils.Func2;

/**
 * Created by andrii on 28/03/2017.
 */

class DetailThumbnailInteractor extends BaseInteractor implements LoaderManager.LoaderCallbacks<Thumbnail> {
    private final Func2<Integer, Loader<Thumbnail>> mThumbnailAsyncLoaderFunc;
    private int mThumbnailId;
    private Action1<Thumbnail> mThumbnailLoadedAction;

    DetailThumbnailInteractor(@NonNull final ResourcesManagerFactory resourcesManagerFactory, @NonNull final Func2<Integer, Loader<Thumbnail>> thumbnailAsyncLoaderFunc) {
        super(resourcesManagerFactory);
        mThumbnailAsyncLoaderFunc = thumbnailAsyncLoaderFunc;
    }

    void loadThumbnail(final int thumbnailId, @NonNull final Action1<Thumbnail> thumbnailLoadedAction) {
        mThumbnailId = thumbnailId;
        mThumbnailLoadedAction = thumbnailLoadedAction;
        getResourcesManagerFactory().getLoaderManager().initLoader(1, null, this);
    }

    @Override
    public Loader<Thumbnail> onCreateLoader(final int id, final Bundle args) {
        return mThumbnailAsyncLoaderFunc.call(mThumbnailId);
    }

    @Override
    public void onLoadFinished(final Loader<Thumbnail> loader, final Thumbnail data) {
        mThumbnailLoadedAction.call(data);
    }

    @Override
    public void onLoaderReset(final Loader<Thumbnail> loader) {
        mThumbnailLoadedAction.call(null);
    }
}
