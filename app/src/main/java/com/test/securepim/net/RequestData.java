package com.test.securepim.net;

import android.net.Uri;
import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import okhttp3.HttpUrl;

/**
 * Created by andrii on 28/03/2017.
 */
public class RequestData {
    @NonNull
    private final String mUrl;
    @NonNull
    private final Map<String, String> params = new HashMap<>();


    private RequestData(@NonNull final String url, @NonNull final Map<String, String> parameters) {
        mUrl = url;
        params.putAll(parameters);
    }

    static RequestData create(@NonNull final HttpUrl httpUrl) {
        final String url = httpUrl.uri().toString();
        final Uri uri = Uri.parse(url);
        final Map<String, String> params = new HashMap<>();
        for (final String key : uri.getQueryParameterNames()) {
            params.put(key, uri.getQueryParameter(key));
        }
        return new RequestData(httpUrl.url().getProtocol() + "://" + httpUrl.url().getHost() + "/", params);
    }

    public static RequestData create(@NonNull String url, Map<String, String> params) {
        return new RequestData(url, params);
    }

    @NonNull
    public Map<String, String> getParams() {
        return params;
    }

    @Override
    public int hashCode() {
        int result = mUrl.hashCode();
        result = 31 * result + params.hashCode();
        return result;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final RequestData that = (RequestData) o;

        if (!mUrl.equals(that.mUrl)) {
            return false;
        }
        return params.equals(that.params);

    }

    @NonNull
    public String getUrl() {
        return mUrl;
    }
}
