package com.test.securepim.net;

import android.support.annotation.NonNull;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import timber.log.Timber;

/**
 * Holds all listeners which observe URLs
 */
@SuppressWarnings("unchecked")
class RequestHolder {
    private final Map<RequestData, List<WeakReference<Callback>>> mRequestingDatas = new HashMap<>();

    <M> void add(@NonNull final RequestData requestData, @NonNull final Callback<M> callback) {
        WeakReference<Callback> weakReference = new WeakReference<Callback>(callback);
        if (mRequestingDatas.containsKey(requestData)) {
            final List<WeakReference<Callback>> callbacks = mRequestingDatas.get(requestData);
            callbacks.add(weakReference);
        } else {
            final ArrayList<WeakReference<Callback>> callbacks = new ArrayList<>(1);
            callbacks.add(weakReference);
            mRequestingDatas.put(requestData, callbacks);
        }
    }

    void remove(@NonNull final RequestData requestData) {
        Timber.d("Size before delete: %d" + mRequestingDatas.size());
        mRequestingDatas.remove(requestData);
        Timber.d("Size after delete: %d" + mRequestingDatas.size());
    }

    <M> void notifyAllOnResponse(@NonNull final RequestData requestData, @NonNull final Call<M> call, @NonNull final retrofit2.Response<M> response) {
        final List<WeakReference<Callback>> callbacks = mRequestingDatas.get(requestData);
        if (callbacks != null) {
            for (final WeakReference<Callback> weakReference : callbacks) {
                final Callback<M> callback = weakReference.get();
                if (callback != null) {
                    callback.onResponse(call, response);
                }
            }
        }
    }

    <M> void notifyAllOnFailure(@NonNull final RequestData requestData, @NonNull final Call<M> call, @NonNull final Throwable t) {
        final List<WeakReference<Callback>> callbacks = mRequestingDatas.get(requestData);
        if (callbacks != null) {
            for (final WeakReference<Callback> weakReference : callbacks) {
                final Callback callback = weakReference.get();
                if (callback != null) {
                    callback.onFailure(call, t);
                }
            }
        }
    }

    <M> void unregister(@NonNull final Map<RequestData, Callback<M>> callbacks) {
        for (final Map.Entry<RequestData, Callback<M>> requestDataCallbackEntry : callbacks.entrySet()) {
            final List<WeakReference<Callback>> weakReferences = mRequestingDatas.get(requestDataCallbackEntry.getKey());
            weakReferences.clear();
        }
    }

    boolean isRequesting(@NonNull final RequestData requestData) {
        return mRequestingDatas.containsKey(requestData);
    }
}
