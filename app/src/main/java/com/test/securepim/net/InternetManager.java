package com.test.securepim.net;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.test.securepim.utils.Func2;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

/**
 * Created by andrii on 27/03/2017.
 */

public class InternetManager {
    private final RequestHolder mRequestHolder = new RequestHolder();

    /**
     * Make request and add data to {@link RequestHolder} with {@param callback} which will be triggered
     * when request is completed
     *
     * @param requestData         with url and data to make request to
     * @param service             interface type for {@link Retrofit}
     * @param serviceCallFunction will trigger request after creating {@link Retrofit}'s service
     * @param callback            {@link Retrofit}'s callback for success or failed response
     * @param <T>                 type of {@link Retrofit}'s service
     * @param <M>                 type of Response
     */
    public <T, M> void makeRequest(@NonNull final RequestData requestData, @NonNull final Class<T> service, @NonNull final Func2<T, Call<M>> serviceCallFunction, @NonNull final Callback<M> callback) {
        Timber.d("makeRequest. callback %s", callback);
        register(requestData, callback);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(requestData.getUrl())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final T t = retrofit.create(service);
        Call<M> call = serviceCallFunction.call(t);
        call.enqueue(new Callback<M>() {
            @Override
            public void onResponse(final Call<M> call, final retrofit2.Response<M> response) {
                final RequestData requestData = RequestData.create(call.request().url());
                Timber.d("Request data received response. Code %d body %s; %s", response.code(), response.body(), requestData.toString());
                mRequestHolder.notifyAllOnResponse(requestData, call, response);
                mRequestHolder.remove(requestData);
            }

            @Override
            public void onFailure(final Call<M> call, final Throwable t) {
                final RequestData requestData = RequestData.create(call.request().url());
                Timber.w("Request data received failed; %s ", requestData.toString());
                mRequestHolder.notifyAllOnFailure(requestData, call, t);
                mRequestHolder.remove(requestData);
            }
        });
    }

    public <M> void register(@NonNull final RequestData requestData, @NonNull final Callback<M> callback) {
        mRequestHolder.add(requestData, callback);
    }

    public <M> void unregister(@Nullable final Map<RequestData, Callback<M>> callback) {
        if (callback != null) {
            mRequestHolder.unregister(callback);
        }
    }

    public boolean isRequesting(@NonNull final RequestData requestData) {
        return mRequestHolder.isRequesting(requestData);
    }
}
