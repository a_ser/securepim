package com.test.securepim.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;


/**
 * Common template for all loaders.
 */
public abstract class AsyncLoader<D> extends AsyncTaskLoader<D> {
    private static final String TAG = AsyncLoader.class.getSimpleName();

    /**
     * Loaded data
     */
    private D data;

    /**
     * Construct loader
     *
     * @param context of application
     */
    public AsyncLoader(final Context context) {
        super(context.getApplicationContext());
    }

    @Override
    public void deliverResult(final D data) {
        if (isReset()) {
            // An async query came in while the loader is stopped
            return;
        }

        this.data = data;

        super.deliverResult(data);
    }


    @Override
    protected void onStartLoading() {
        if (data != null) {
            deliverResult(data);
        }

        if (takeContentChanged() || data == null) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        // Attempt to cancel the current load task if possible.
        cancelLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();

        // Ensure the loader is stopped
        onStopLoading();

        data = null;
    }

}