package com.test.securepim.base.view;

/**
 * Created by andrii on 27/03/2017.
 */

public interface IView {
    void manageLoadingVisibility(boolean isVisible);
}
