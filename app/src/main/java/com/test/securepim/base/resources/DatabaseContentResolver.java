package com.test.securepim.base.resources;

import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;

/**
 * Created by andrii on 27/03/2017.
 */

public class DatabaseContentResolver {
    private Context mContext;

    public DatabaseContentResolver(final Context context) {
        mContext = context;
    }

    public void registerContentObserver(final Uri uri, final boolean des, final ContentObserver observer) {
        mContext.getContentResolver().registerContentObserver(uri, des, observer);
    }

    public void unregisterContentObserver(final ContentObserver observer) {
        mContext.getContentResolver().unregisterContentObserver(observer);
    }

    public void clearAndInsert(final Uri uri, final ContentValues[] values) {
        //TODO thread?
        mContext.getContentResolver().delete(uri, null, null);
        mContext.getContentResolver().bulkInsert(uri, values);
    }
}
