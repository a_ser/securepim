package com.test.securepim.base.view;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.test.securepim.base.presenter.IPresenter;
import com.test.securepim.base.resources.ResourcesManagerFactory;


/**
 * Created by andrii on 27/03/2017.
 */

public abstract class BaseFragment<P extends IPresenter, V extends IView> extends Fragment implements IView, Router {
    private ResourcesManagerFactory mResourcesManagerFactory;
    @Nullable
    private P mPresenter;

    @Override
    public void manageLoadingVisibility(final boolean isVisible) {
    }

    @Override
    @CallSuper
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mResourcesManagerFactory = new ResourcesManagerFactory(this);
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initPresenter(savedInstanceState);
        checkNotNull(mPresenter, "You should initialise presenter");
    }

    protected abstract void initPresenter(@Nullable Bundle savedInstanceState);

    protected static void checkNotNull(@Nullable final Object object, @NonNull final String msg) {
        if (object == null) {
            throw new NullPointerException(msg);
        }
    }


    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().setTitle(getTitle() != null ? getTitle() : "");
    }

    @Nullable
    protected abstract String getTitle();

    @Nullable
    public P getPresenter() {
        return mPresenter;
    }

    protected void setPresenter(@NonNull final P presenter) {
        mPresenter = presenter;
    }

    public ResourcesManagerFactory getResourcesManagerFactory() {
        return mResourcesManagerFactory;
    }

    @Override
    public void navigateToFragment(@NonNull final Fragment fragment) {
        ((Router) getActivity()).navigateToFragment(fragment);
    }

}
