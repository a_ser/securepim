package com.test.securepim.base.interactor;

import android.support.annotation.NonNull;

import com.test.securepim.base.resources.ResourcesManagerFactory;


/**
 * Created by andrii on 27/03/2017.
 */

public abstract class BaseInteractor {
    private final ResourcesManagerFactory mResourcesManagerFactory;

    protected BaseInteractor(@NonNull final ResourcesManagerFactory resourcesManagerFactory) {
        mResourcesManagerFactory = resourcesManagerFactory;
    }

    @NonNull
    public ResourcesManagerFactory getResourcesManagerFactory() {
        return mResourcesManagerFactory;
    }
}
