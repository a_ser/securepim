package com.test.securepim.base.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.test.securepim.base.view.IView;

/**
 * Created by andrii on 27/03/2017.
 */

public abstract class BasePresenter<V extends IView> implements IPresenter {
    @Nullable
    protected V mView;

    public BasePresenter(@NonNull final V view) {
        mView = view;
    }

    @Override
    public void onDestroyView() {
        mView = null;
    }

    protected void managetLoadingVisibility(final boolean isVisible) {
        if (mView != null) {
            mView.manageLoadingVisibility(isVisible);
        }
    }
}
