package com.test.securepim.base.resources;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;

import com.test.securepim.MainApplication;
import com.test.securepim.net.InternetManager;

/**
 * Created by andrii on 27/03/2017.
 */

public class ResourcesManagerFactory {
    private final MainApplication mContext;
    private final LoaderManager mLoaderManager;

    public ResourcesManagerFactory(@NonNull final Fragment fragment) {
        mContext = (MainApplication) fragment.getContext().getApplicationContext();
        mLoaderManager = fragment.getLoaderManager();
    }

    public DatabaseContentResolver getContentResolver() {
        return mContext.getDatabaseContentResolver();
    }

    public InternetManager getInternetManager() {
        return mContext.getInternetManager();
    }

    public LoaderManager getLoaderManager() {
        return mLoaderManager;
    }
}
