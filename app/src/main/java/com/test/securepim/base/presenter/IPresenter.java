package com.test.securepim.base.presenter;

/**
 * Created by andrii on 27/03/2017.
 */

public interface IPresenter {

    void onDestroyView();
}
