package com.test.securepim.base.view;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

/**
 * Created by andrii on 28/03/2017.
 */

public interface Router {

    void navigateToFragment(@NonNull Fragment fragment);
}
