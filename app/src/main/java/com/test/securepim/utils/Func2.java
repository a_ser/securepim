package com.test.securepim.utils;

/**
 * Created by andrii on 27/03/2017.
 */

public interface Func2<T, R> {
    R call(T r);
}
