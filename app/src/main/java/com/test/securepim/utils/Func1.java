package com.test.securepim.utils;

/**
 * Created by andrii on 27/03/2017.
 */

public interface Func1<T> {
    T call();
}
