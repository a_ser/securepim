package com.test.securepim.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.test.securepim.db.ThumbnailsContract;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by andrii on 27/03/2017.
 */

public class Thumbnail {
    @SerializedName("previewHeight")
    @Expose
    public Integer previewHeight;
    @SerializedName("likes")
    @Expose
    public Integer likes;
    @SerializedName("favorites")
    @Expose
    public Integer favorites;
    @SerializedName("tags")
    @Expose
    public String tags;
    @SerializedName("webformatHeight")
    @Expose
    public Integer webformatHeight;
    @SerializedName("views")
    @Expose
    public Integer views;
    @SerializedName("webformatWidth")
    @Expose
    public Integer webformatWidth;
    @SerializedName("previewWidth")
    @Expose
    public Integer previewWidth;
    @SerializedName("comments")
    @Expose
    public Integer comments;
    @SerializedName("downloads")
    @Expose
    public Integer downloads;
    @SerializedName("pageURL")
    @Expose
    public String pageURL;
    @SerializedName("previewURL")
    @Expose
    public String previewURL;
    @SerializedName("webformatURL")
    @Expose
    public String webformatURL;
    @SerializedName("imageWidth")
    @Expose
    public Integer imageWidth;
    @SerializedName("user_id")
    @Expose
    public Integer userId;
    @SerializedName("user")
    @Expose
    public String user;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("userImageURL")
    @Expose
    public String userImageURL;
    @SerializedName("imageHeight")
    @Expose
    public Integer imageHeight;

    public static Thumbnail create(@NonNull final Cursor query) {
        final Thumbnail thumbnail = new Thumbnail();
        thumbnail.webformatURL = query.getString(query.getColumnIndex(ThumbnailsContract.ThumbnailsEntry.COLUMN_WEBFORM_URL));
        thumbnail.user = query.getString(query.getColumnIndex(ThumbnailsContract.ThumbnailsEntry.COLUMN_USER));
        thumbnail.id = query.getInt(query.getColumnIndex(ThumbnailsContract.ThumbnailsEntry.COLUMN_ID));
        thumbnail.tags = query.getString(query.getColumnIndex(ThumbnailsContract.ThumbnailsEntry.COLUMN_TAGS));
        thumbnail.likes = query.getInt(query.getColumnIndex(ThumbnailsContract.ThumbnailsEntry.COLUMN_LIKES));
        thumbnail.favorites = query.getInt(query.getColumnIndex(ThumbnailsContract.ThumbnailsEntry.COLUMN_FAVORITES));
        thumbnail.comments = query.getInt(query.getColumnIndex(ThumbnailsContract.ThumbnailsEntry.COLUMN_COMMENTS_COUNT));
        thumbnail.userImageURL = query.getString(query.getColumnIndex(ThumbnailsContract.ThumbnailsEntry.COLUMN_USER_IMAGE_URL));
        return thumbnail;
    }

    @NonNull
    ContentValues getContentValues() {
        final ContentValues contentValues = new ContentValues();
        contentValues.put(ThumbnailsContract.ThumbnailsEntry.COLUMN_WEBFORM_URL, webformatURL);
        contentValues.put(ThumbnailsContract.ThumbnailsEntry.COLUMN_USER, user);
        contentValues.put(ThumbnailsContract.ThumbnailsEntry.COLUMN_ID, id);
        contentValues.put(ThumbnailsContract.ThumbnailsEntry.COLUMN_TAGS, tags);
        contentValues.put(ThumbnailsContract.ThumbnailsEntry.COLUMN_LIKES, likes);
        contentValues.put(ThumbnailsContract.ThumbnailsEntry.COLUMN_FAVORITES, favorites);
        contentValues.put(ThumbnailsContract.ThumbnailsEntry.COLUMN_COMMENTS_COUNT, comments);
        contentValues.put(ThumbnailsContract.ThumbnailsEntry.COLUMN_USER_IMAGE_URL, userImageURL);
        return contentValues;
    }

    @NonNull
    public String getFormattedTags() {
        StringBuilder sb = new StringBuilder();
        for (final String s : getSplittedTags()) {
            sb.append("#").append(s).append(" ");
        }
        return sb.toString();
    }

    private List<String> getSplittedTags() {
        List<String> tagsList = new ArrayList<>();
        StringTokenizer stringTokenizer = new StringTokenizer(tags, ",");
        do {
            tagsList.add(stringTokenizer.nextToken().trim());
        } while (stringTokenizer.hasMoreTokens());
        return tagsList;
    }
}
