package com.test.securepim.models;

import android.content.ContentValues;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by andrii on 27/03/2017.
 */

public class PixabayResponse {
    @SerializedName("hits")
    @Expose
    private List<Thumbnail> hits = null;

    @Nullable
    public ContentValues[] getValues() {
        ContentValues[] values = null;
        if (hits != null) {
            values = new ContentValues[hits.size()];
            for (int i = 0; i < hits.size(); i++) {
                values[i] = hits.get(i).getContentValues();
            }
        }
        return values;
    }
}
