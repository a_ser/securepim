package com.test.securepim.db;

/**
 * Created by andrii on 10.02.15.
 */

import android.content.res.AssetManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

class SqlParser {

    @Nullable
    static List<String> parseSqlFile(final String sqlFile, @NonNull final AssetManager assetManager) throws IOException {
        List<String> sqlIns = null;
        final InputStream is = assetManager.open(sqlFile);
        try {
            sqlIns = parseSqlFile(is);
        } finally {
            is.close();
        }
        return sqlIns;
    }

    @NonNull
    private static List<String> parseSqlFile(@NonNull final InputStream is) throws IOException {
        final String script = removeComments(is);
        return splitSqlScript(script, ';');
    }

    @NonNull
    private static String removeComments(@NonNull final InputStream is) throws IOException {

        final StringBuilder sql = new StringBuilder();

        final InputStreamReader isReader = new InputStreamReader(is);
        try {
            final BufferedReader buffReader = new BufferedReader(isReader);
            try {
                String line;
                String multiLineComment = null;
                while ((line = buffReader.readLine()) != null) {
                    line = line.trim();

                    if (multiLineComment == null) {
                        if (line.startsWith("/*")) {
                            if (!line.endsWith("}")) {
                                multiLineComment = "/*";
                            }
                        } else if (line.startsWith("{")) {
                            if (!line.endsWith("}")) {
                                multiLineComment = "{";
                            }
                        } else if (!line.startsWith("--") && !line.equals("")) {
                            sql.append(line);
                        }
                    } else if (multiLineComment.equals("/*")) {
                        if (line.endsWith("*/")) {
                            multiLineComment = null;
                        }
                    } else if (multiLineComment.equals("{")) {
                        if (line.endsWith("}")) {
                            multiLineComment = null;
                        }
                    }

                }
            } finally {
                buffReader.close();
            }

        } finally {
            isReader.close();
        }

        return sql.toString();
    }

    @NonNull
    private static List<String> splitSqlScript(@NonNull final String script, final char delim) {
        final List<String> statements = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        boolean inLiteral = false;
        final char[] content = script.toCharArray();
        for (int i = 0; i < script.length(); i++) {
            if (content[i] == '\'') {
                inLiteral = !inLiteral;
            }
            if (content[i] == delim && !inLiteral) {
                if (sb.length() > 0) {
                    statements.add(sb.toString().trim());
                    sb = new StringBuilder();
                }
            } else {
                sb.append(content[i]);
            }
        }
        if (sb.length() > 0) {
            statements.add(sb.toString().trim());
        }
        return statements;
    }
}