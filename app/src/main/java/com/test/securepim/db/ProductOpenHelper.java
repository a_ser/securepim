package com.test.securepim.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.util.List;

import timber.log.Timber;


/**
 * Allow to roll updates from version to version
 * Upgrade files names should looks like: upgrade-{version}.sql. Example upgrade-24.sql
 * Best approach is using {version} as build number of app, when database was changed
 */
public class ProductOpenHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "main_database";
    private static final int DATABASE_VERSION = 1;

    private static final String SQL_DIR = "sql";

    private static final String CREATEFILE = "create.sql";

    private static final String UPGRADEFILE_PREFIX = "upgrade-";

    private static final String UPGRADEFILE_SUFFIX = ".sql";
    private static SQLiteOpenHelper sInstance;

    private final Context mContext;

    private ProductOpenHelper(final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context.getApplicationContext();
    }

    public static SQLiteOpenHelper getInstance(final Context context) {
        if (sInstance == null) {
            sInstance = new ProductOpenHelper(context);
        }
        return sInstance;
    }

    @Override
    public void onCreate(final SQLiteDatabase db) {
        try {
            execSqlFile(CREATEFILE, db);
        } catch (IOException exception) {
            throw new RuntimeException("Database creation failed", exception);
        }
    }


    @Override
    public void onUpgrade(@NonNull final SQLiteDatabase db, final int oldVersion, final int newVersion) {
        Timber.d("Upgrading schema from version " + oldVersion + " to " + newVersion + "");
        try {
            Timber.d("upgrade database from " + oldVersion + " to " + newVersion);
            for (final String sqlFile : AssetsUtils.list(SQL_DIR, mContext.getAssets())) {
                if (sqlFile.startsWith(UPGRADEFILE_PREFIX)) {
                    final int fileVersion = Integer.parseInt(sqlFile.substring(UPGRADEFILE_PREFIX.length(), sqlFile.length() - UPGRADEFILE_SUFFIX.length()));
                    if (fileVersion > oldVersion && fileVersion <= newVersion) {
                        execSqlFile(sqlFile, db);
                    }
                }
            }
        } catch (final IOException exception) {
            throw new RuntimeException("Database upgrade failed", exception);
        }
    }

    private void execSqlFile(final String sqlFile, @NonNull final SQLiteDatabase db) throws IOException {
        Timber.d("exec sql file: {}" + sqlFile);
        final List<String> commands = SqlParser.parseSqlFile(SQL_DIR + "/" + sqlFile, mContext.getAssets());
        if (commands != null) {
            for (final String sqlInstruction : commands) {
                Timber.d("sql: {}" + sqlInstruction);

                db.execSQL(sqlInstruction);
            }
        }
    }
}