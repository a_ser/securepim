package com.test.securepim.db;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by andrii on 27/03/2017.
 */

public interface ThumbnailsContract {
    String CONTENT_AUTHORITY = "com.test.securepim";
    Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    String PATH_THUMBNAILS = "thumbnails";

    Uri THUMBNAILS_URI = Uri.parse(BASE_CONTENT_URI + "/" + PATH_THUMBNAILS);
    Uri THUMBNAILS_URI_ITEM = Uri.parse(BASE_CONTENT_URI + "/" + PATH_THUMBNAILS + "/#");

    interface ThumbnailsEntry extends BaseColumns {
        String TABLE_NAME = "Thumbnails";
        String COLUMN_LIKES = "likes";
        String COLUMN_FAVORITES = "favorites";
        String COLUMN_TAGS = "tags";
        String COLUMN_VIEWS = "views";
        String COLUMN_COMMENTS_COUNT = "comments_count";
        String COLUMN_WEBFORM_URL = "webformat_url";
        String COLUMN_USER_IMAGE_URL = "user_image_url";
        String COLUMN_USER = "user";
        String COLUMN_ID = "image_id";
    }
}
