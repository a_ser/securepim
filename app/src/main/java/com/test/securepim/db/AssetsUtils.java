package com.test.securepim.db;

/**
 * Created by andrii on 10.02.15.
 */

import android.content.res.AssetManager;

import java.io.IOException;
import java.util.Arrays;

final class AssetsUtils {
    private AssetsUtils() {
        // Utils
    }

    static String[] list(final String path, final AssetManager assetManager) throws IOException {
        final String[] files = assetManager.list(path);
        Arrays.sort(files);
        return files;
    }
}
