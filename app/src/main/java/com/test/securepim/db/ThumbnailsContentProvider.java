package com.test.securepim.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.test.securepim.MainApplication;

import timber.log.Timber;

public class ThumbnailsContentProvider extends ContentProvider {
    public static final String URI_THUMBNAILS = "vnd.android.cursor.dir/vnd."
            + ThumbnailsContract.CONTENT_AUTHORITY + "." + ThumbnailsContract.ThumbnailsEntry.TABLE_NAME;
    public static final String URI_THUMBNAILS_ID = "vnd.android.cursor.item/vnd"
            + ThumbnailsContract.CONTENT_AUTHORITY + "." + ThumbnailsContract.ThumbnailsEntry.TABLE_NAME;
    private static final UriMatcher sUriMatcher;
    private static final int THUMBNAILS_TABLE = 1;
    private static final int THUMBNAILS_TABLE_ID = 2;

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(ThumbnailsContract.CONTENT_AUTHORITY, ThumbnailsContract.PATH_THUMBNAILS, THUMBNAILS_TABLE);
        sUriMatcher.addURI(ThumbnailsContract.CONTENT_AUTHORITY, ThumbnailsContract.PATH_THUMBNAILS + "/*", THUMBNAILS_TABLE_ID);
    }

    private SQLiteOpenHelper mSQLiteOpenHelper;

    public ThumbnailsContentProvider() {
    }

    @Override
    public boolean onCreate() {
        final Context context = getContext();
        if (context != null) {
            mSQLiteOpenHelper = ((MainApplication) context).getDatabase();
            return true;
        }
        return false;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String type = getType(uri);
        switch (type) {
            case URI_THUMBNAILS:
                return mSQLiteOpenHelper.getReadableDatabase().query(
                        ThumbnailsContract.ThumbnailsEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
            case URI_THUMBNAILS_ID:
                break;

        }
        throw new UnsupportedOperationException("Unknown query for uri: " + uri.toString());
    }

    @NonNull
    @Override
    public String getType(@NonNull Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case THUMBNAILS_TABLE:
                return URI_THUMBNAILS;
            case THUMBNAILS_TABLE_ID:
                return URI_THUMBNAILS_ID;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri.toString());
        }
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        switch (getType(uri)) {
            case URI_THUMBNAILS:
                mSQLiteOpenHelper.getWritableDatabase().insert(ThumbnailsContract.ThumbnailsEntry.TABLE_NAME, null, values);
                notifyUri(ThumbnailsContract.THUMBNAILS_URI);
                Timber.d("Item insert %s ", values.toString());
                return ThumbnailsContract.THUMBNAILS_URI;
            case URI_THUMBNAILS_ID:

            default:
                throw new UnsupportedOperationException("Unknown insert operation for uri: " + uri.toString());
        }
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        switch (getType(uri)) {
            case URI_THUMBNAILS:
                Timber.d("Item delete");
                final int delete = mSQLiteOpenHelper.getWritableDatabase().delete(ThumbnailsContract.ThumbnailsEntry.TABLE_NAME, selection, selectionArgs);
                notifyUri(ThumbnailsContract.THUMBNAILS_URI);
                return delete;
            default:
                throw new UnsupportedOperationException("Unknown delete operation for uri: " + uri.toString());
        }
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void notifyUri(@NonNull Uri uri) {
        if (getContext() != null && getContext().getContentResolver() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
    }
}
