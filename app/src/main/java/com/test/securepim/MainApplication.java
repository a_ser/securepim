package com.test.securepim;

import android.app.Application;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.test.securepim.base.resources.DatabaseContentResolver;
import com.test.securepim.db.ProductOpenHelper;
import com.test.securepim.net.InternetManager;

import timber.log.Timber;

/**
 * Created by andrii on 27/03/2017.
 */

public class MainApplication extends Application {
    private SQLiteOpenHelper mDatabaseHelper;
    private InternetManager mInternetManager;
    private DatabaseContentResolver mDatabaseContentResolver;

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    @NonNull
    public SQLiteOpenHelper getDatabase() {
        if (mDatabaseHelper == null) {
            synchronized (MainApplication.class) {
                if (mDatabaseHelper == null) {
                    mDatabaseHelper = ProductOpenHelper.getInstance(this);
                }
            }
        }
        return mDatabaseHelper;
    }

    @NonNull
    public InternetManager getInternetManager() {
        if (mInternetManager == null) {
            synchronized (MainApplication.class) {
                if (mInternetManager == null) {
                    mInternetManager = new InternetManager();
                }
            }
        }
        return mInternetManager;
    }

    public DatabaseContentResolver getDatabaseContentResolver() {
        if (mDatabaseContentResolver == null) {
            synchronized (MainApplication.class) {
                if (mDatabaseContentResolver == null) {
                    mDatabaseContentResolver = new DatabaseContentResolver(getApplicationContext());
                }
            }
        }
        return mDatabaseContentResolver;
    }
}
