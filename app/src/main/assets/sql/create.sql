CREATE TABLE IF NOT EXISTS  Thumbnails (
    _id INTEGER PRIMARY KEY NOT NULL,
    comments_count INTEGER,
    favorites  INTEGER,
    image_id INTEGER,
    likes INTEGER,
    tags TEXT,
    user TEXT,
    user_image_url TEXT,
    views INTEGER,
    webformat_url TEXT
);